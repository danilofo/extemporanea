## Collaborazioni extemporanee

---

## Back to the future
Riflessioni introduttive

-

## Libertà è decentralizzazione
Uno dei più longevi percorsi di Scienceground è la <span class= "fragment highlight-red">conoscenza critica delle tecnologie informatiche</span>

<span class="fragment highlight-green"> Open source e decentralizzazione</span>

-

<img src="./images/decentralized.png"  width="1000" height="600">

-

<img src="./images/decentralized_serious.png" width="1000" height="400">

-

## Do It Yourself

<span class="fragment fade-in">
<ul>
	<span class="fragment highlight-red">
		<li>Autonomia tecnica</li>
	</span>
	<span class="fragment highlight-green">
	    <li>Flessibilità</li>
	</span>
	<li>Contiene i costi</li>
</ul>
</span>

note: I piani a pagamento tendono a essere economici solo per le funzioni di base

-

## La flessibilità è la chiave

- L'infrastruttura è complessa anche per un piccolo gruppo!  L'ecosistema tecnico è in continuo mutamento...

- Approccio: cambi incrementali e selezione naturale
  (qualche esempio più tardi)

---

## Comunicazione interna

-

### ![Matrix.org](https://matrix.org)

- Open standard (interoperabilità!) e architettura decentralizzata
- Stanze per chat in stile Slack

-

### Element (fu Riot.im)

- Gratuito, ma su un server unico <p class="fragment fade-in">(_de facto_ centralizzato)</p>
- Supporto per chiamate e videoconferenze 
  <span class="fragment highlight-green">
  <span class="fragment fade-in">Novità!</span></span>

---

## Archiviazione e collaborazione

Per ragioni <span style="color:orange">**economiche**</span> distinguiamo dati in testuali/ non testuali (foto, video, audio)

-

### "Formato" Markdown

- [Non è un "formato"](https://www.markdownguide.org/): puro testo

- Potente: sintassi semplificata per l'`html`

- Editing collaborativo: [CodiMD](demo.codimd.org)

In futuro potremmo averne un'istanza <span style="color:violet">**direttamente sul nostro server**</span>

-

### Gitlab

- Controllo di versione (VC): salvare l'intera storia dei documenti! 

- Da [`git`](https://git-scm.com/) alle web IDE: CodiMD e [Gitlab](https://gitlab.com/extemporanea)

- Il VPS scaricherà automaticamente tutta la directory - e i dati VC - per ulteriore sicurezza!

-

### Documentazione tecnica

Wiki, templates, linee guida, note per gli amministratori del server  ecc. 

-

### Bibliografie
[Zotero](https://zotero.org)

---

## Esempio di workflow
1. Evviva, un nuovo progetto!

Chi è il <span style="color:purple">**REFERENTE**</span>? **X**

-

2. **X** crea una nuova cartella relativa al progetto su [Gitlab](https://gitlab.com/extemporanea)

**Si può usare il sito** o la riga di comando (`git`)

-

3. **X** crea una nuova stanza su [Element](riot.im)

-

4. **X** Invita tutti a scrivere su [CodiMD](demo.codimd.org) condividendo il link su Element

-

5. Creatività extemporanea collettiva: vengono scritti materiali e documentazione

-

6. <span style="color:orange">**Tutti**</span> possono aggiornare i dati salvati su Gitlab.

<span class="fragment fade-in-then-semi-out">

**X** ora può concentrarsi sui <span style="color:red"> messaggi minatori</span> per finire il progetto (e salvare una volta ogni tanto)!
</span>

---

Fatto!
<section data-background="http://i.giphy.com/90F8aUepslB84.gif"></section>

---

## Esempi

-

### Case study 1: formato testi

<p class="fragment fade-in-then-semi-out">Vogliamo avere un metodo semplice di condividere testi</p>

<p class="fragment fade-in">
Puro testo + formattazione Markdown
</p>
 
<p class="fragment fade-in">
Economico e virtualmente immortale, libertà nella scelta dell'editor, no codifiche proprietarie, integrazione con VC</p>

<span class="fragment fade-in">
	<span class="fragment highlight-red">Si è rivelato utile per l'archiviazione!</span>
</span>


-

### Case study 2: archivio unico testi
<span class="fragment fade-in-then-semi-out">
Vogliamo il controllo di versione
</span>

<span class="fragment fade-in">

- il VPS o Gitlab o entrambi (**ridondanza**) 

- Soluzione centralizzata: <span style="color:orange">unico server CodiMD </span>
  <span class="fragment fade-in-then-semi-out " style="color:red"> Svantaggi: upgrade VPS, sicurezza dei dati</span>

- Soluzione decentralizzata: <span style="color:orange">editing `!=` salvataggio</span>
  CodiMD come editor e VC a breve termine, Gitlab a lungo termine
  
<span class="fragment fade-in-then-semi-out" style="color:red">Necessita di formazione collettiva</span>
</span>

-

### Case study 3: Sostituire Wordpress

Optiamo per un altro strumento (conoscete [Hugo](gohugo.io)?)
 
 - Il nostro VPS ci rende flessibili (rispetto allo shared hosting, magari Wordpress.com...)
 
 - Esportare e reimportare gli articoli: avere articoli già formattati in Markdown semplificare questo passaggio **ad altissimo rischio**

---

<section data-background-iframe="https://scienceground.it" data-background-interactive>
<div style="position: absolute; width: 40%; right: 0; box-shadow: 0 1px 4px rgba(0,0,0,0.5), 0 5px 25px rgba(0,0,0,0.2); background-color: rgba(0, 0, 0, 0.9); color: #fff; padding: 20px; font-size: 20px; text-align: left;">
						<h2>Il nostro blog</h2>

---

### Wordpress

>è una piattaforma software di "blog" e content management system (CMS) open source ovvero un programma che, girando lato server, consente la creazione e distribuzione di un sito Internet formato da contenuti testuali o multimediali, gestibili ed aggiornabili in maniera dinamica.
>
> Fonte: Wikipedia

-

### Hosting 

- Soluzioni _self-hosted_ vs _hosted_

- Virtual private server (VPS): [Linode](https://linode.com)

Permette il backup completo e automatico dell'intero server (è già servito!)

---

## Fine!
 - Questa presentazione è scritta in Markdown! 
 
 - Il rendering usa [Reveal.js](https://revealjs.com)

 - Su Gitlab trovate le [istruzioni per riprodurla](https://gitlab.com/danilofo/extemporanea/-/blob/master/2020_scuola/strumenti_tecnici/)
