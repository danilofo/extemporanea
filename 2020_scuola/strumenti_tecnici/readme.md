# Interactive presentations using Reveal.js and Markdown

# Quick Start

## install nodejs and npm
```
$ apt install nodejs npm
```

## download and install reveal.js 
```
$ git clone https://github.com/hakimel/reveal.js.git
$ cd reveal.js && npm install
```

## modify the index.html file (and possibly provide the source markdown and image directory as symlinks) NOTE: do not change the filenames here!
```
$ mv my_source_dir/index.html index.html
$ ln -s my_source_dir/source.md source.md
$ ln -s my_source_dir/images/ images
```

## start the local web server and share your screen
```
$ npm start
```
then visit http://localhost:8000

# Thorough documentation
Follow the [original guide](https://revealjs.com/installation/#full-setup)
