# Algoritmo umano pandemico




### Militarizzazione


- I paesini intorno a Codogno vivono una sorta di occupazione militare: l'esercito è ovunque e gli spostamenti sono bloccati. Un treno verso l'Austria ~~pochi giorni fa~~ è stato fermato per più di due ore al Brennero, chi era a bordo obbligato a una visita medica. Le misure d'emergenza si moltiplicano. Le università di Padova e Bologna stanno valutando l'insegnamento telematico in questo semestre. Le manifestazioni femministe per l'8 marzo in molte città del nord sono seriamente a rischio divieto. Cos'è un'emergenza? Come valutiamo quello che ci toglie? Può davvero essere una misura temporanea? Cosa contiene questo momento di non temporaneo e durevole?




- La Cina che ha chiuso città e militarizzato ospedali è il primo produttore mondiale di dispositivi di "sicurezza", che in questi giorni sono [usciti allo scoperto](https://global.ilmanifesto.it/chinas-first-health-emergency-in-the-artificial-intelligence-era/). Secondo alla Cina è Israele, il cui ministro Yisrael Katz, all'indomani degli attentati di Bruxelles, ha [apostrofato i belgi](https://www.haaretz.com/israel-news/israeli-minister-focus-on-muslims-not-chocolate-1.5421771) perché "mangiano troppa cioccolata" e non si concentrano sulla minaccia terroristica islamica. Assieme a armi, droni, sensori e altre amenità, è infatti necessario esportare l'ideologia securitaria. Un'azione che, come [argomenta l'attivista israeliano Jeff Halper](http://scienceground.it/en/science-war-and-society-jeff-halper/), sta riconfigurando il concetto stesso di guerra, da scontro di potenze a controllo di quella vasta porzione di umanità che è marginale o aliena ai sistemi di produzione e consumo di merci e servizi. All'esterno, con le "azioni" e le "missioni", e all'interno, con la militarizzazione delle forze di polizia e il dispiegamento dell'esercito in contesti civili (molto del quale a solo scopo dimostrativo e "rassicurante"). Tuttavia è facile prevedere che l'altrettanto prevedibile fallimento del contenimento del virus, invece di esser preso come dimostrazione dell'impossibilità di controllare batteri, funghi, muffe, e l'80% della popolazione mondiale, verrà usato dai lobbisti dell'industria della sicurezza per aumentare la pressione sui paesi deboli e influenzabili come l'Italia a dotarsi di ulteriori strumentazioni della guerra securitaria.



###  Due etti di rischio. Che faccio, lascio?

- Il dibattito scientifico non si svolge sulle colonne del Corriere della Sera. [Tuttavia] in questi giorni il volto di un noto virologo è diventato quasi un santino da apporre con scaramanzia su ogni articolo dei giornali online. I video in cui sembra ora rassicurare, ora chiamare al sacrificio nazionale, ce lo rassomigliano a un presidente della Repubblica in tempo di guerra, [di fatto facendo le veci di istituzioni evidentemente troppo deboli per poter affermare una linea (siamo in tempi di "uomo forte"?)]. Il suo ruolo è, chiaramente, quello dello scienziato chiamato a rispondere ai dubbi della società civile. Ma possiamo dire che, in quel frangente, si comporti "come uno scienziato"? ~~Sosteniamo di no, e ci chiediamo come sia fatto qualcuno che si~~ Com'è fatta una che si comporta "come uno scienziato"? [questa domanda non ha una risposta all'interno dell'articolo]
 
- ~~Innanzitutto, le richieste che arrivano ad uno scienziato possono variare di molto.~~ Nel 2015 il gruppo editoriale di *Nature* ospitava un [dibattito](https://www.nature.com/news/engineered-bat-virus-stirs-debate-over-risky-research-1.18787) sull'eticità di alcune ricerche riguardanti i salti di specie (*spillover*) dei virus da animali a uomo, che di fatto rischiano proprio di indurre quel salto. Negli ultimi anni, la ricerca sui coronavirus è stata oggetto di tagli ai finanziamenti, con conseguente riduzione della [dignità di pubblicazione](http://www.smj.org.sg/sites/default/files/Ed-2020-042-epub.pdf). Oggi, nell' "emergenza", potrebbe aumentare la pressione a condurre queste stesse ricerche, se per esempio necessarie allo sviluppo di un vaccino. [Matteo: a questo paragrafo manca una punchline]

- Poi, la matematica del contagio è una questione molto complessa. [L'articolo di Paolo Giordano](https://www.corriere.it/cronache/20_febbraio_25/matematicadel-contagioche-ci-aiutaa-ragionarein-mezzo-caos-3ddfefc6-5810-11ea-a2d7-f1bec9902bd3.shtml) e [quello di Andrea Capocci](https://ilmanifesto.it/r0-corso-accelerato-di-epidemiologia/) si concentrano, con toni differenti, sulla descrizione di un parametro che gli epidemiologi usano per caratterizzare il numero medio di persone che un infetto contagia durante un'epidemia. In poche parole, entrambi si concentrano su un modello molto studiato, il SIR, in base al quale si può stimare l'evoluzione di un evento epidemico. 
Il ragionamento, nella sua versione più semplificata, prevede una massa infinita di persone suscettibili che interagiscono uniformemente con tutte le altre, trasmettendo la malattia con una certa probabilità. In questo caso, il parametro fondamentale diventa il numero medio di nuovi contagi che ogni infetto causa. Se questo numero è maggiore di uno, il numero di malati crescerà all'infinito, mentre se è minore l'epidemia si estinguerà rapidamente. Tuttavia, questo racconto si basa su ipotesi non verificate nelle epidemie reali, che coinvolgono un numero finito di persone e, soprattutto, delle reti sociali profondamente eterogenee. La ricerca in epidemiologia teorica è molto attiva e questo fa sì che ci sia anche un acceso dibattito su questioni basilari [Danilo: io qui metterei il link alla review di vespignani https://journals.aps.org.sci-hub.tw/rmp/abstract/10.1103/RevModPhys.87.925]. Il dibattito coinvolge, solo per fare qualche esempio:
(1) i modelli più adatti a descrivere un'evoluzione epidemica. Questo punto incide drasticamente sul successivo
(2) l'identificazione di una soglia epidemica, ossia predire quali caratteristiche di un'epidemia permettono di prevedere se si estinguerà, e quale frazione di popolazione avrà contagiato
(3) le migliori strategie di immunizzazione: per un'immunizzazione ottimale, i pazienti da vaccinare sono gli stessi che contagerebbero più persone.
Modelli sempre più particolareggiati possono includere aspetti della realtà che quelli precedenti astraevano puntando a guadagnare in semplicità e generalità, e possono argomentare contro l'intuizione guadagnata dai precedenti modelli, a cui pure si ispirano. Per esempio, è il caso del dibattito [sulle restrizioni alla mobilità](https://arxiv.org/abs/1908.05261)[Danilo: se troviamo anche qualche articolo pubblicato è meglio...].
[Inoltre la stima del valore di questo parametro nelle prime fasi dell'epidemia è fortemente influenzata dal fatto che la ricostruzione dei contagi viene fatta tracciando gli avvenimenti pregressi (la famigerata ricerca del paziente zero), dal fatto che si usa il tempo di manifestazione (visibile) invece che il tempo di contagio (invisibile), e da vari altri effetti statistici confondenti. Tutti insieme questi effetti portano a _sottostimare_ il valore di questo famoso parametro - vale a dire, l'effettiva diffusione del virus. Quindi no, la matematica del contagio non è semplice, ammesso che ne esista una.][Danilo: se vogliamo includere questa riflessione occorre basarsi su fonti credibili]
Come dice Alessandro Vespignani [nell'intervista a Il Tascabile](https://www.iltascabile.com/scienze/predire-le-epidemie/) in una epidemia è impossibile separare la sfera della conoscenza da quella dell'azione e, diciamo noi, della politica. Questo perché nel momento in cui si usa un certo modello per fare delle predizioni su cui valutare delle misure, relative per esempio al tipo di quarantena da adottare o il numero di test da somministrare, le ipotesi alla base del modello, che non possono essere testate, retroagiscono sulla situazione _successiva_ all'applicazione delle misure, e contribuiscono dunque a creare quella realtà che il modello doveva originariamente descrivere. 
Potremmo dire che tutto questo non è caratteristico solo delle epidemiologia più che del resto della "cultura", come è ben descritto da Bruno Latour in [Non siamo mai stati moderni](https://www.ibs.it/non-siamo-mai-stati-moderni-libro-bruno-latour/e/9788833020297). 






~~Nei giorni scorsi Burioni ha detto, tra le molte cose, uno strafalcione statistico: ha parlato della percentuale di infettati che sono finiti in rianimazione, omettendo però di dire che nelle fasi iniziale questa percentuale non può che essere fortemente sovrastimata, perché appunto è più probabile rilevare i casi sintomatici o fortemente sintomatici. Burioni, epidemiologo, non poteva ignorare di star dicendo una inesattezza, di forzare i dati in maniera spinta. Lo ha fatto con un preciso intento politico (ripetiamolo: ogni discorso sulla scienza è politico) e non dovremmo limitarci a evidenziare il suo gesto, ma cercare di coglierne l'intento per capire anche su quali punti di forza si appoggia.~~

- Larga parte del dibattito e delle "opinioni degli esperti", va nella direzione di NON mettere in dubbio che c'è un'emergenza. La reale minaccia del virus non può essere dubitata: dobbiamo sempre aspettarci il peggio e diffidare di chi sminuisce. La macchia infernale dell'infezione si sposta al di là di ogni considerazione concreta sugli effetti, è un postulato, un oggetto primitivo. Il morbo diventa quasi un'idea astratta, la precipitazione qui e ora di una potenzialità negativa presente in ogni legame umano, in ogni esperienza di vita.
Dare al virus un carattere di male imponderabile, da prevenire a ogni costo, significa che governare e governarsi sono l'unico aspetto saliente della politica (e della Scienza?) [questa frase secondo me potrebbe essere riformulata]. Nessuno spazio è lasciato ai sillogismi e alle implicazioni. SE c'è la quarantena ALLORA non prendo lo stipendio, non posso muovermi, non posso festeggiare il carnevale, non posso andare a manifestare. Quel “se... allora...” è la traccia di un concatenamento, e quindi di una possibile messa in discussione che lo scientismo “à la Burioni” [almeno qui lo lasciamo? che sinonimo possiamo trovare?] [@danilo: propongo scientismo emergenziale, perché trascende Burioni - vale per l'articolo di Paolo Giordano, ad esempio - ed è chiaramente una chimera solo ammantata di retorica scientista] vuole tranciare: c'è solo il virus, e le misure necessarie che il virus _ci impone_.

- L'emergenza non può essere una certezza nemmeno in senso lato: dal punto di vista dei "potenti", non può esserlo perché il pericolo riguarda primariamente anziani e malati, i deboli della società la cui emergenza è iniziata allora già da tempo, con i tagli ai servizi sociali e sanitari che sono il vero rischio di questa epidemia. Inoltre, come ci spiega Diamond in [*Armi, acciaio, malattie*](https://www.einaudi.it/catalogo-libri/storia/storia-antica/armi-acciaio-e-malattie-jared-diamond-9788806219222/), i germi sono alleati dell'uomo bianco da tempo e gli hanno consentito la "supremazia" su vari popoli di raccoglitori e cacciatori attorno al globo. Si potrebbe pensare dunque che la selezione operata dai microrganismi sia positiva a lungo termine per il progresso della specie umana, per esempio consentendoci di arrivare più preparati ai futuri sviluppi dell'Antropocene. 
Dal punto di vista degli scienziati, [virus e ospiti coevolvono](https://onlinelibrary.wiley.com/doi/full/10.1002/ajpa.10384) e forse gran parte delle nostre difese immunitarie derivano da questa storia comune. Vale la pena dunque bloccare completamente l'assoggettamento a queste pressioni selettive? 
Da un punto di vista ecosistemico, infine, variazioni nella numerosità e "qualità" delle popolazioni, negli equilibri, sono normali, e dunque c'è poco da preoccuparsi. 


[questo qui sotto è già detto e secondo me si può togliere]
- Scrive _ipse_ che tra otto giorni vedremo gli effetti delle misure messe in atto ora. Ma come faremo a distinguere questo effetto da una banale _regression to the mean_ statistica, o da mille altre concause e fattori confondenti? Potremo mai riprodurre l'esperimento più e più volte uguale, in condizioni di laboratorio, per assicurarci davvero che quella conseguenza derivasse da quella causa? Seguendo il principio di falsificazione, qual è la predizione esatta proposta da _ipse_, e che se confutata squalifica l'ipotesi che "gli effetti" che osserveremo tra una settimana siano dovuti a queste particolari misure? Perché se tra una settimana, qualsiasi effetto si palesasse, ancora questo "verificasse" l'efficacia delle misure, allora avremmo un chiaro esempio che "la scienza" (come se ce ne fosse una...) viene qui usata come discorso per _imporre_ a posteriori la causa ad una certa conseguenza, e quindi uno strumento di esercizio di potere.





### Né col virus, né coi carcerieri 




- Appellarsi agli esperti affinché comunichino il rischio senza allarmismi ma senza sacrificare il rigore scientifico nasconde quindi delle insidie. 
Che siate convinti delle scelte fatte, oppure no, in ogni caso non c'è nessun automatismo [determinismo?], nessun paradigma oggettivamente corretto che può essere eseguito più o meno bene. Se noi dobbiamo “ascoltare gli esperti”, gli esperti, loro, chi ascoltano? Come pesano i modelli di “sicurezza” e “igiene”? Su quale bilancia valoriale e esistenziale?
Il discorso economicista si intreccia rapidamente con quello scientifico. Quanto "conviene" una certa misura? La questione è probabilmente la più interessante. Quando gli esperti parlano di una certa decisione (per esempio, quarantene per spostare il picco epidemico di qualche settimana, magari senza incidere sul numero totale dei contagiati ma dando tempo agli ospedali di riorganizzarsi, al prezzo di scambi commerciali rallentati e produttività abbattuta e costi per le famiglie) come fanno a valutare le alternative? Su quali modelli si basano? Quali considerazioni pratiche li guidano, ossia cosa ritengono possibile e cosa no? Un gesto non è valido di per sé, può essere legittimato da una giustificazione scientifica. Una pandemia può risolversi se la popolazione attua le pratiche "giuste" (lavarsi le mani, evitare troppi contatti) anche se non ne conosce bene le motivazioni, purché vengano da una fonte "autorevole" (gli esperti). Se gli strumenti per comprendere certe risposte sono dati a tutti, la risoluzione dell'emergenza dovrebbe essere ancora più rapida. Ma in che misura vanno respinti i gesti che appaiono inconsulti (ad esempio, quelli scaramantici), se però sono richiesti dalla maggioranza? Se sono benefici su piani diversi da quello scientifico o medico, possono essere ricompresi nei protocolli? 
Al di là dei proclami, _non è vero che si faccia di tutto pur di contenere il virus_. Se accettiamo questo, allora diventa una necessità fare in modo che le azioni contro l'epidemia non ci facciano cadere nell'universo disciplinare richiamato da [Wu Ming](https://www.wumingfoundation.com/giap/2020/02/diaro-virale-1/) e [Agamben](https://www.quodlibet.it/giorgio-agamben-l-invenzione-di-un-epidemia) (ovvero non sempre basta [chiudere delle porte per avere una casa sicura](https://www.solipsia.it/2019/10/30/che-cosa-e-casa/?fbclid=IwAR0n71cS8T7UAq2OTUkq06ly1jPkTW5Di3SJvHDJI6j_7icIL3-eb9nUJTo))


- L'altra questione intimamente legata a quella economica è quella dei dati usati nei modelli epidemiologici. La raccolta dati non è discussa democraticamente anche se coinvolge l'intera società, e insieme agli indubbi benefici logistici porta con sé anticipazioni su futuri inquietanti. 
La valutazione scientifica del rischio, se analizzata da vicino, finisce per restituirci una realtà complessa, che contiene le questioni sociali da cui avevamo cercato di astrarci.




- Certo tutto questo ragionamento può sembrare teorico, astratto. Le sentiamo le risposte: «ok, tutto è relativo, nemmeno la scienza è oggettiva. Però qua ci troviamo di fronte a un rischio sanitario quindi possiamo dire che “ciò che vogliamo” è talmente chiaro, talmente uguale per tutti, da essere considerato oggettivo ed universale, e quindi anche le misure da prendere in conseguenza di questa emergenza». 
Ma perché?
Ogni dicotomia (dicotomico esprime una divisione di concetti antitetici, divisori) tra sicurezza e libertà (così come tra salute e libertà) ci pone già un problema, perché ~~ci mostra l'insufficienza di entrambi i concetti~~ concetti tanto vasti non possono essere de-finiti in maniera isolata, senza essere messi in relazione. Il coronavirus sembrerebbe un caso limite in cui siamo obbligati a scontrarci con delle presunte “barriere naturali” alla libertà. 

~~L'effetto del virus sembra per ora analogo a quello di una sindrome influenzale, sia in termini di sintomi che in termini di mortalità. Resta il fatto che alcune fasce della popolazione sono particolarmente colpite, e la novità del morbo induce una pressione inaspettata sul sistema sanitario.~~
Pensiamo che questo sia il modo sbagliato di guardare il problema. Il dibattito pubblico si muove dentro il presupposto che la paura e l'emergenza vadano commisurate a un rischio "valutato correttamente". Ma questa valutazione è impossibile se insistiamo a cercare nel rischio una causa che "di per sé" giustifichi gli effetti normativi.
Invece osserviamo che gli automatismi governamentali (che riguardano i governi ma anche l'apparato mediatico e certi aspetti del senso comune) tendono in situazione d'emergenza a riprodurre e potenziare alcune premesse dell'attuale convivenza sociale, senza discuterle: che ogni interazione umana ha una potenzialità patologica e che quindi sia necessario governare le interazioni umane più di ogni altra cosa. Questo avviene anche se un senso comune scettico esiste eccome, e ha una sua profonda legittimità, come riscostruito sempre nel diario dei [WuMing](https://www.wumingfoundation.com/giap/2020/02/diaro-virale-1/)

### E quindi?

- La scienza che vorremmo partecipa a una cosmologia di tipo diverso~~. In cui i concatenamenti sono prioritari.~~ In cui le domande sono prioritarie sulle risposte.
Proviamo a dirne alcune. Fermare il virus o contenerne gli effetti? A quale costo? A spese di chi? Con quale tipo di emozioni devo fare tutto questo? Esiste qualcosa che ha rischi paragonabili al virus ma che per ora ha scatenato molta meno paura? (Spoiler: sì esiste! L'impennata di tumori dovuta all'inquinamento dell'aria, il rischio di una futura inefficacia antibiotica dovuta all'abuso che ne facciamo, non sono che alcuni esempi). Attraverso queste domande riusciamo così a rimettere in primo piano la zona di indistinguibilità tra strumenti tecnico-scientifici e orizzonti etici. A rimettere al centro il nostro cosmo.

- La domanda più importante la lasciamo per ultima: cos'è il rischio? Sembra diventato il principale compito degli "esperti" quello di fornirci una giusta stima del rischio. Ma è proprio nell'esistenza di un rischio che si posiziona la nostra esperienza del mondo: solo dando un valore a certi eventi riusciamo a parlare di _rischio_. La statistica - cioè la costruzione del rapporto tra un insieme di eventi auspicabili e l'insieme degli eventi possibili - nasconde nella delimitazione di questi due insiemi un'operazione per nulla neutrale. A cosa siamo disposti a rinunciare per contenere un'epidemia i cui effetti sono poco chiari? Cosa prendiamo in considerazione quando si tratta di "contenimento del rischio"? Come mai il blocco della socialità aggregativa improduttiva è più discusso che il blocco della produzione? Vediamo in controluce una catena di "necessità sociali" che contribuiscono a produrre il discorso sul rischio, e però rimandano in ogni momento lo spazio per chiederci cosa riteniamo desiderabile e bello. [qui forse si potrebbe fare anche solo un esempio tipo "città del messico dopo il terremoto" o "new orleans dopo kathrina". Ma solo con carattere esemplare, senza scendere nel dettaglio]
Le tecniche scientifiche nella loro materialità, sono invece prima di tutto un modo per costruire collettivamente un senso dentro all'imponderabile, per darci delle chiavi di lettura del il mondo, e magari affrontare problemi vecchi come la vita stessa (il pericolo, il connettersi col diverso, la morte). 


- Ci sembra di poter concludere che l'emergenza ha un carattere ibrido, in cui considerazioni scientifiche, economiche, sociali, idee su come il mondo è e come dovrebbe o potrebbe essere, concorrono a formare le idee che troviamo sui giornali e sulle reti sociali; a loro volta queste formano le idee che i cittadini si fanno sulla scienza e sulla politica, e queste retroagiscono a monte, riattivando il ciclo. Ci si può dunque chiedere cosa differenzia l'emergenza virus da altre "emergenze" che stiamo vivendo contemporaneamente. La risposta è, come ci aspettiamo, molteplice e sfaccettata: un virus attinge a un immaginario zombie comune al [mondo occidentale](https://not.neroeditions.com/la-croce-della-pandemia/) e al tempo stesso si propaga rapidamente. Il senso di impotenza c'è ma è minore, la Scienza conserva il suo ruolo di guida morale, o almeno tenta di farlo; questo anche perché è più facile piegare un'epidemia alle necessità produttive del mondo moderno rispetto alla crisi climatica: più facile controllare il sociale e l'organizzazione lavorativa della vita domestica che far fronte alla minaccia di diminuzione della produttività del pianeta.





>  [Come diceva Danilo, anche secondo me a questo punto questo paragrafo è superfluo] [oppure potrebbe essere anticipato?] 

- La ricostruzione giornalistica narra di Lodi e Codogno come epicentro di un'epidemia che si diffonde altrove. Tuttavia, la stessa diffusione potrebbe essere spiegabile come bias cognitivo, "effetto ottico" dovuto al campionamento: con tanta parte della popolazione già infetta, il paziente 1 potrebbe semplicemente essere il primo ad aver manifestato sintomi gravi, e la malattia si manifesterebbe per puro effetto statistico dove vengono eseguiti i tamponi. Ecco allora che la ricerca del paziente 0 e di tutte le possibili linee di trasmissione tramite contatti personali diventano forse buone come trama per un racconto di Borges e poco più, anche perché viziate da mille incognite e fattori confondenti (_we should all have something to hide_, scriveva brillantemente [Moxie](https://moxie.org/blog/we-should-all-have-something-to-hide/), e _indeed_ [invero?] tanta parte delle nostre vite sono sommerse: frequentazioni, abitudini, episodi di cui intenzionalmente o inconsciamente ci potremmo dimenticare quando sottoposti a un'indagine). Con il rischio reale di creare narrazioni tossiche stabili (finita l'emergenza, chi si preoccuperà di concludere questa ricerca e restituire verità storica all'intera vicenda?). Questa ricerca ossessiva di un segnale nel rumore, di un nesso causale, una spiegazione, indica quanto ancora la nostra concezione dell'epistemologia di scienze complesse come l'epidemiologia sia ancella del metodo riduzionista delle cosidette "scienze esatte", caratterizzate dall'istituzione di meccanismi e nessi causali. Il più delle volte però i metodi statistici utilizzabili in epidemiologia si riducono ad arginare il dubbio tramite procedure il più possibile asettiche, e non a individuare meccanismi e soluzioni. Ecco allora che una pratica più solida ~~- anche e meno morbosamente _sexy_ -~~ potrebbe essere quella di eseguire tamponi campionando la popolazione su tutto il territorio nazionale, magari con una distribuzione variabile, crescente in prossimità di focolai noti. Se così facendo si trovassero dei positivi in zone disconnesse secondo le narrazioni causalistiche, ci si metterebbe il cuore in pace e si concentrerebbero gli sforzi sull'assistenza ai casi gravi.



### Refs.


https://www.facebook.com/spaziocatai/photos/a.781513818660339/1931058027039240

https://not.neroeditions.com/la-croce-della-pandemia/


https://global.ilmanifesto.it/chinas-first-health-emergency-in-the-artificial-intelligence-era/

https://hurriya.noblogs.org/post/2020/02/23/dalle-frontiere-citta-alcune-riflessioni-stati-demergenza-controllo-sociale/

https://www.corriere.it/cronache/20_febbraio_25/matematicadel-contagioche-ci-aiutaa-ragionarein-mezzo-caos-3ddfefc6-5810-11ea-a2d7-f1bec9902bd3.shtml

https://ilmanifesto.it/r0-corso-accelerato-di-epidemiologia/

https://arxiv.org/abs/1908.05261

https://www.iltascabile.com/scienze/predire-le-epidemie/

https://www.wumingfoundation.com/giap/2020/02/diaro-virale-1/

https://www.quodlibet.it/giorgio-agamben-l-invenzione-di-un-epidemia

https://not.neroeditions.com/la-croce-della-pandemia/

https://www.nature.com/news/engineered-bat-virus-stirs-debate-over-risky-research-1.18787

http://www.smj.org.sg/sites/default/files/Ed-2020-042-epub.pdf

https://onlinelibrary.wiley.com/doi/full/10.1002/ajpa.10384

https://www.solipsia.it/2019/10/30/che-cosa-e-casa/?fbclid=IwAR0n71cS8T7UAq2OTUkq06ly1jPkTW5Di3SJvHDJI6j_7icIL3-eb9nUJTo