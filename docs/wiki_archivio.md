# Introduzione

L'archivio di eXtemporanea è basato sulla piattaforma pCloud, un servizio di file hosing online simile a Dropbox, ma più vicino ai nostri principi (enfasi posta sulla riservatezza, risponde alle leggi svizzere sulla privacy) e alle nostre esigenze (ha un sistema avanzato di collaborazione, permette di guardare video e ascoltare file audio direttamente dall'interfaccia, offre delle API gratuite). \
Concretamente, l'archivio è una repository di file, con cartelle e sottocartelle, ed un file di indice alla radice che contiene brevi descrizioni dei contenuti, per orientare un'eventuale ricerca.

Al momento, l'archivio è ospitato in parte sul Dropbox di Matteo, in parte su una soluzione di macchine condivise decentralizzate (cioè ognuno ha un po' di roba a caso, senza un ordine preciso). Al momento di migrare verso pCloud, bisognerà raccogliere tutti i documenti sparsi per l'Europa e catalogarli, secondo il metodo nuovo.

# Istruzioni per archiviare materiale

La gerarchia di archiviazione per dei documenti è prima cronologica, poi per evento. 

>  	`Esempio`
>
>La cartella "/archivio/2019/" contiene tutti i progetti iniziati e finiti nel 2019.


### Indice

--TODO--

Alla radice dell'archivio si trova un documento che serve da indice; che sia una mappa concettuale che riprende tutte le attività di scienceground o un semplice albero della struttura dell'archivio, il suo scopo è essere consultato all'inizio di una ricerca all'interno dell'archivio stesso.

--TODO--

### Biblioteca e bibliografia globale
Fanno eccezione  alla regola cronologica i materiali non originali, cioè libri, articoli e altro materiale bibliografico. Sia per evitare di disperdere questo tipo di materiale che per potervi accedere facilmente, questo andrà raccolto tutto in una sezione dedicata, la cartella /Biblioteca/.\
 Per completezza, nella sezione bibliografica vanno indicate anche risorse che abbiamo utilizzato ma che non possediamo;
 
 (creare un unico grosso file extemporanea.bib e mantenerlo aggiornato: --TODO--).


### Progetti
Ogni cartella di progetto deve contenere il materiale prodotto per/durante/in occasione del progetto. Questo riguarda documenti scritti, in Markdown quando possibile, fotografie, video o podcast.\
 Resta inteso che se si lavora ad un progetto multimediale, i file *raw* inutili non devono fare parte dell'archivio; se si vogliono conservare, questi si tengono su un hard disk, e se ce n'è bisogno, si trasferiscono fra gli interessati con sistemi adatti (Filemail, TransferXL, automobile + HDD nel bagagliaio...). Va menzionata la loro esistenza, e chi li conserva, nel file README del progetto. \
Ogni cartella di progetto deve contenere dei metadati, in particolare un file README e una bibliografia:

#### README
 Per mantenere una repository ordinata, accessibile e utilizzabile da un numero di persone superiore a φ, si usano i file README. Noi non facciamo eccezione. Ad ogni cartella relativa ad un evento bisogna aggiungere un file di testo riassuntivo, README.txt, che deve contenere una breve descrizione dell'evento, i riferimenti di chi vi ha partecipato, eventuali dettagli che un futuro visitatore dei documenti avrebbe bisogno di conoscere.\
 Un file README deve dare ad un lettore esterno un quadro ben definito del progetto senza essere troppo preciso, e deve essere, senza sacrificare troppi dettagli, *breve*.

>  	`Esempio`
>
>Progetto: Data Mining, 2018.\
>Partecipanti: Stefano Lacaprara, Carlo Emilio Montanari, Lavinia Marziale, Gianni Petrella.
>
>Svolgimento: abbiamo confrontato un pubblico di ~20 ragazzi ai problemi tipici del data mining: cercare informazioni specifiche e pertinenti su internet, filtrarle per pertinenza, fare calcoli e stime grossolane.
>
>Materiale archiviato:\
> Non ci sono registrazioni dell'evento. Questo dossier contiene una lista degli esempi di problemi di data mining che sono stati proposti al pubblico.

#### Bibliografia di progetto
Affiancata ad ogni file README deve trovarsi una bibliografia per il progetto. Dato che le risorse bibliografiche **NON** vanno archiviate insieme al progetto a loro relativo, ma nella sezione a loro dedicata, avere un file bibliografico nella descrizione di un progetto è l'unico modo per sapere quali documenti sono stati utilizzati per il progetto stesso!
  	
	  
	Notare che i titoli riportati nelle bibliografie di ogni progetto devono trovarsi ANCHE nella bibliografia principale. (automatizzare sincronizzazione con API --TODO--)

## Eventi ripetuti

Se un progetto si ripete più volte, il materiale della prima versione va archiviato secondo l'ordine standard. Le edizioni successive, invece, devono riportare nel file README un messaggio che specifichi chiaramente "questo evento è una replica/adattamento dell'evento precedente", con un riferimento alla positzione della cartella originale, e la cartella della replica deve contenere unicamente i file relativi alla nuova versione (nuove foto, nuovi risultati, interviste a gente nuova ecc.)

## Eventi permanenti

Se un progetto non ha una fine precisa, come ad esempio un podcast, una radio o una newsletter (*cough cough*), la sua cartella si trova nella subdirectory /Progetti permanenti/ .

# Nomenclatura documenti

I documenti di qualsiasi tipo, quindi incluse foto, video, audio ecc., DEVONO avere nomi significativi, a discrezione di chi li scrive: NON si archiviano documenti con nome "AUX_20200811", "IMG_12_12_2019_04", "note", "varie", "asjbhflvhbawrivnar", " final_final_2" e simili, nell'interesse della sanità mentale di tutti. Per cortesia.


#### 