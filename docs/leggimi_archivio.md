NOTA: Questo documento, privo di questa riga, si trova nel root dell'archivio pCLoud.

# Archivio

Questa è una breve introduzione all'archivio di eXtemporanea. Il materiale è organizzato per anno, ad eccezione di alcuni tipi di materiale e dei progetti permanenti; il materiale bibliografico, per esempio, non va archiviato in ordine cronologico ma in un'unica cartella, /Archivio/Biblioteca/.

Per avere tutti i dettagli sul metodo di archiviazione (e sull'idea del futuro metodo di lavoro di eXtemporanea), abbiamo scritto delle wiki dedicate, che riportiamo qui nella cartella /Archivio/wiki/


 Per aggiungere materiale a questo archivio, è necessario accedervi dal link apposito (cosa che avete appena fatto, se state leggendo questo), ed avere un account pCloud. Uno gratuito è sufficente, basta un indirizzo mail valido per crearne uno. In alternativa, è possibile mandare un'email a chi di dovere (*sapete* a chi mandarla).

 In futuro, aggiungeremo misure di sicurezza per accedere all'archivio e per modificarlo. Per il momento, ci affidiamo al buonsenso di chi ha il link. Non fate casino!


 Link archivio: 