Minimal notes on managing a server to self-host a website without knowing much about it
---------------------------------------------------------------------------------------

Chapter 0. Where I decline all responsibility
---------------------------------------------

CAUTION (_RADIATION AREA_): The drafter of the following document is **not** an expert sysadmin. Taking everything with (more than just) a grain of salt is required while chewing this notes. **You have been warned.**

Elaborating a bit on the previous point, the author is definitely wrong in the following cases:
1. If [Linode][docs] says so
2. If the [Arch Wiki][arch] or the [Debian Wiki][deb] say so; but trust the Arch Wiki the most, in particular about Apache 2 and PHP related stuff
3. If a random guy on [StackOverflow][so] inspires you more trust than the aforementioned anonymous and barely skilled drafter

The assumptions behind this notes are as follows:
1. Everybody uses GNU/Linux, and by this I mean Debian-based distros, and by this I mean the last LTS release of Ubuntu. I live in a universe where that's the only possible choice, just don't try to convince of this the people in the Arch community.
2. Everybody has a working internet connection
3. Everybody is familiar with the following shell commands (familiarity meaning: knowing 'everything' in the related `man` page): 
	```
	man
	ls, mv, cp
	su
	sudo
	chmod
	man (seriously)
	```
	By continuing reading, you also acknowledge that the `rm` command is no longer valid shell syntax, and that you should use `rm -i` instead.

4. Everybody knows everything about `bash` wildcards.
5. Everybody cares about (a minimal amount of) security.
6. Everybody cares about backups.

Finally, this guide was written and is updated according to an unscrutable time table and an idiosyncratic logic. You're free to complete it yourself if you think it doesn't say something important and you feel like writing it.

[docs]:https://www.linode.com/docs/
[arch]:https://wiki.archlinux.org/
[deb]:https://wiki.debian.org/
[so]:https://stackoverflow.com/

Chapter 1. What's Linode and how to deal with it
------------------------------------------------
### 1.1 Hosting services

TODO!

### 1.2 How to manage the account

TODO!

Note that in order to have a LAMP stack + Wordpress correctly installed I used a pre-prepared Debian + Wordpress image.

### 1.3 What we pay for at the moment

We pay for a basic account (Linode Nanode, 5$/month, VAT excluded) with a modest 1 core and 25 GB of storage. We also have an automatic backup service on (2$/mo, VAT excluded).  

### 1.4 Emergency measures: don't push the red button!

The Linode dashboard is useful in emergency situations. 

BONUS FAQ: "I would like to experiment with it on my own, but _for free_ FFS!"  
You can (at the moment, at least)! Just follow the best advice ever and [**READ THE DOCS**][gs]; you will find a discount code that covers your first two months of experimentation.
This is highly recommended, since there is an informal statistics telling us that the first three attempts to access the Linode dashboard to read the settings end up destroying all the data.

[gs]:https://www.linode.com/docs/getting-started/

Chapter 2. What's our VPS (Virtual Private Server)
--------------------------------------------------

### 2.1 A note about history and traditions

Servers are generally named after planets (like Jupyter or Mars) or philosophers (Plato...).
Ours' name is Latour. Only a guild of carefully chosen people know for sure the name of the admin account. For the others there's nothing left but to guess...

### 2.2 The OS 

Currently, we are using Debian 10 Buster (Stable). Note that our `/etc/apt/source.list` (don't change it!) is tracking Stable, so the system gets updated anytime a new release is out. Beware.
Here is what Latour has to say about it
```
bruno@latour:~$ uname -a
Linux latour 4.9.0-9-amd64 #1 SMP Debian 4.9.168-1+deb9u3 (2019-06-16) x86_64 GNU/Linux

bruno@latour:~$ less /etc/apt/source.list
# only relevant output shown
deb http://mirrors.linode.com/debian stable main
deb-src http://mirrors.linode.com/debian stable  main

deb http://mirrors.linode.com/debian-security stable/updates main
deb-src http://mirrors.linode.com/debian-security stable/updates main

```

### 2.3 The software we use

- `unattended-upgrades`:
  security upgrades are automatically installed.
- `fail2ban`:
  bans remote hosts that try to connect to the server after 2 failed attempts. Might need some effort to work at full efficiency. 
- `lynis`:
  Security auditing system, is not installed via the `apt` package manager. Instead, you will find it at `/etc/bruno/mynis` as a git repo. This means, just issue
  ```
  bruno@latour:~$ git pull
  ```
  before `sudo ./lynis audit system`.
- `git`: you **know** why. Otherwise, consider it a dependency for `lynis`.
- a LAMP stack, but more on this in Chapter 4
- Wordpress, for which you might want to read Chapter 6

Chapter 3. Conversations with Latour
------------------------------------
If you don't get the joke, please read Chapter 2.

### 3.1 How to summon Latour, or `ssh myuser@scienceground.it`
  NOTE: this only works after your user has been setup properly (password authentication enabled, or ssh key transferred on `latour`)
  
  Change `myuser` with your user. 
  Save a configuration in `.ssh/config` to summon Latour using `ssh bruno@latour`(if you can control the power of `bruno`)
  ```
  # .ssh/config
  ################
  Host latour
      Hostname scienceground.it
      User bruno
  ```

### 3.2 How to move stuff around, or `scp source target`
Only works if you have ssh access.

Examples
```
# from local computer to user@latour
scp local/path/myfile user@latour:remote/path/
# from latour to local
scp user@latour:remote/path/ local/path/myfile
```
Now you got it. If not, read `man scp`.

### 3.3 Security considerations

TODO!

### 3.4 How to deal with ssh keys and obtain unlimited powers

ACHTUNG: don't start this if you don't have an admin nearby.
TODO!

Chapter 4. What's a LAMP stack
------------------------------

TODO!

Chapter 5. SSL and https
------------------------

TODO!

For now we don't have an SSL certificate, but it's relatively easy using [EFF Let's Encrypt][certbot] and we plan to do it. It takes one evening, but I need to be sober...

[certbot]:https://letsencrypt.org/

Chapter 6. Wordpress
--------------------

TODO!

Chapter 7. Security
-------------------

TODO!

- **lynis**

- **rkhunter**

- **fail2ban**
config fail: /etc/fail2ban/jail.conf

Chapter 8. Archive
------------------

TODO!

Chapter X. The neverending story of what could be next
------------------------------------------------------

1. A serious archive
--------------------
While Linode offers the possibility to expand the storage capacity as needed (just add a new volume, and appropriately configure /etc/fstab) it costs a bit (0.1$/GB per month). A dedicated storage solution is offered, for example, by [rsync.net][rsync]. I recommend looking into [`borg`][borg] as a software for the archive.

[rsync]:https://rsync.net/
[borg]:https://www.borgbackup.org/

2. Improved security
--------------------
Someone who knows better should really look into this mess.
