# Guida breve all'uso di Gitlab

Gitlab è una piattaforma di lavoro condiviso, il cui obiettivo è implementare un sistema online di controllo versione del lavoro.\
In pratica, il suo funzionamento è simile ad una cartella condivisa qualsiasi: se io lavoro ad un progetto, creo una repository dedicata su Gitlab e carico i file che ho in questo momento. Durante una settimana modifico i file sul mio computer, e fra una settimana aggiornerò i file su Gitlab con le modifiche fatte nel frattempo. In questo modo, non solo posso lavorare sul mio computer senza dovermi affidare a piattaforme online, ma quando ho una versione provvisoria del mio lavoro la posso pubblicare e renderla accessibile agli altri.

## Ma allora, in cosa è diverso da una cartella condivisa qualsiasi?

La differenza fondamentale, che è la vera forza di Gitlab, è il sistema di commit.

Un commit è il modo in cui si aggiornano file sulla repository, si possono lasciare commenti sulle differenze fra una versione e l'altra del file e, più importante ancora, si possono mantenere controlli di versione. Aggiungere e modificare file è meno semplice che su Dropbox, per dire, ma ne vale la pena.

## Come funziona Gitlab?

Un progetto, su Gitlab, funziona nel modo seguente:

1. Creo una cartella per il mio progetto.\
Per fare ciò, bisogna andare sulla pagina della repository extemporanea, aprire il Web IDE e creare una cartella dove si desidera. Per un progetto nuovo, la soluzione più semplice è crearla nel *root* della repository. 

2. Nella cartella appena creata, si aggiungono i file relativi al progetto, cliccando sul tasto *upload file*.

3. Una volta caricato il materiale, bisogna fare un commit: fortunatamente, l'interfaccia di Gitlab *prepara* i commit automaticamente, quindi tutto ciò che bisogna fare è cliccare sul tasto Commit. Se non sapete a cosa mi riferisco, non preoccupatevi; se lo sapete, smettete di leggere questa guida, non ne avete bisogno.

4. Una volta creato il commit, ci si troverà davanti un ingresso per il commit message, cioè per qualche frase esplicativa della modifica che si sta facendo, e un'opzione "push to master" o "create a new branch". Questo è il momento in cui, se sapete esattamente cosa state facendo e siete autorizzati, potete selezionare "master branch", altrimenti create un branch nuovo, possibilmente che porti il vostro nome e/o il nome del progetto, e scegliete quello. NON selezionate l'opzione "create merge request", quella vi servirà dopo.

Confermate il commit. Congratulazioni, avete appena fatto il primo commit del progetto!


## Cos'è un branch? Che cosa ho appena creato?
Si può pensare ai vari branch come a dei quaderni di brutta copia: il branch master è la "bella", la versione principale, mentre i branch secondari sono versioni "provvisorie". Se io ho creato delle modifiche al progetto, ma non sono sicuro che siano pronte per andare nella versione principale, pusherò le mie modifiche sul mio branch. In questo modo, il mio lavoro sarà tutto salvato sul mio branch, senza rischiare di interferire con la versione "bella", né con le modifiche degli altri.


## Come faccio ad aggiornare il mio lavoro?
Una volta che il primo commit del progetto è stato fatto, per aggiornarlo bisogna, sempre usando il Web IDE,
1. Scegliere il branch giusto, quello su cui il progetto è stato avviato al punto precedente.
2. Aggiungere i file nuovi e le nuove versioni dei file già esistenti. Si possono anche cancellare file, se servisse.
3. Creare un nuovo commit, spiegando le modifiche fatte e selezionando il branch corretto, sempre SENZA creare merge request.


## Cos'è una merge request? Come si passa un lavoro finito sulla versione principale?
Una volta che il lavoro sarà presentabile, si creerà una richiesta di fusione alla versione principale, una *merge request*. Questa, una volta accettata dall'amministratore della repository (cioè Danilo o Gianni), trasferirà le modifiche esistenti sul branch master, e il progetto sarà trasferito dal branch temporaneo alla "bella copia".
