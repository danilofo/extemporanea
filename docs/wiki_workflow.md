## Modello di workflow per eXtemporanea

L'attività de eXtemporanea è strutturata per forza di cose per progetti: ogni attività a cui partecipiamo, sia questa la nostra presenza in festival più grandi, oppure laboratori da due ore ripetibili in qualsiasi liceo, o ancora una newsletter o una zine, è a sé stante, e pertanto il modello di workflow che proponiamo segue questo principio.

## Workflow

1. Un progetto nuovo viene proposto. Si sceglie un responsabile (o due responsabili) che si occuperà della coordinazione dei lavori ed assume i seguenti compiti:
	- Creare una cartella per il progetto nella repository Gitlab.
	- Compilare il file README.txt del progetto (cfr wiki archivio).
	- Creare una bibligrafia relativa al progetto e mantenerla aggiornata (cfr wiki archivio).

2. Il responsabile 
	- crea una stanza Element per discutere del progetto,
	-  invita con gentilezza ma fermamente i collaboratori ad accedervi,
	- aggiunge la stanza creata alla community extemporanea, che raccoglie tutte le stanze esistenti.

3. Il responsabile si concentra sulla spedizione di messaggi minatori ai suoi collaboratori, volti ad assicurare il buon ritmo di lavoro ed un'atmosfera conviviale.

4. Il progetto si può portare avanti su varie piattaforme -quella che consigliamo è CodiMD; sta al responsabile assicurarsi di fare commit regolari e significativi nella cartella del progetto, mantenere aggiornati i metadati del progetto (README e bibliografia) e saper utilizzare un minimo Gitlab (cfr wiki gitlab).

5. Archiviazione:\
Una volta terminato il progetto, il responsabile si assicura di avere le ultime versioni di ogni file, prende l'intera cartella del progetto e la archivia su pCloud, o chiede a chi ha l'account di pCloud di farlo.

## Punti da risolvere:
- Durante il lavoro del progetto, si possono tenere file multimediali nella repository. Una volta il tutto archiviato, questi vanno rimossi?