# FAQ Element


#### Che cos'è Element?

Element, fu Riot.im, è il sistema di messaggistica istantanea, condivisione file e videoconferenze che utilizziamo. È simile ad altri sistemi di messaggistica, come Messenger/Whatsapp/Telegram/ecc, ma è più vicino ai nostri ideali e alle nostre necessità.

#### Non so cosa sia un motore di ricerca, come faccio ad usare Element anche io?

https://element.io 

#### Ma utilizzarlo dal computer è scomodo!

Esistono versioni ufficiali di Element per Android, iOS, Windows, Mac e Linux: https://element.io/get-started \
Bisogna però notare che le nuove features, come l'implementazione delle community, vengono rese disponibili prima sul client per computer, e dopo sulle versioni mobili (al momento, da Android e iOS non è possibile accedere alle community!).


#### Ci sono troppe stanze!

https://knowyourmeme.com/photos/1650747

#### Ho altre domande, a chi posso porle?

A Gianni o a Danilo. :)