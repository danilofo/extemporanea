# Written in Python 3.8 by Gianni Petrella
#
# To use this, just run 
#
# python3 signaturegen.py
#
# or
#
# python signaturegen.py
#
# in a shell.



import random as rd

def generate(low = 1, high = 10):
	"""
	Generates HTML string for the signature of eXtemporanea. Changes font and size randomly.
	Input: low, high: integers that represent the lower and upper bound for the size of the caracters.
	"""
	with open('fonts.txt') as f:
		fonts = f.read().split('\n')

	out = '<p>E<span style=\"font-family: \''
	out = out + rd.choice(fonts) + '\','
	if rd.randint(0,1) == 1:
		out = out + ' sans-serif;'
	out = out + ' font-size: ' + str(rd.randint(low, high)) + 'em;\">X</span>TEMPORANEA</p>'

	return out

if __name__ == "__main__":
	print(generate())